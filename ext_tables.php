<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'TROTZDEM e.V Template');

t3lib_div::loadTCA('tt_content');

t3lib_extMgm::addPageTSConfig('  
	mod.SHARED.colPos_list = 0,1,2,3,4,5  
');  

$TCA['tt_content']['columns']['colPos']['config']['items'] = array (  
	'1' => array ('Links||Links||||||||','1'),  
	'0' => array ('Mitte||Mitte||||||||','0'),  
	'3' => array ('Rechts||Rechts||||||||','3'),  
	'2' => array ('Unten Links||Unten Links||||||||','2'),  
	'4' => array ('Unten Rechts||Unten Rechts||||||||','4'),  
	'5' => array ('Fusszeile||Fusszeile||||||||','5'),
	'5' => array ('Fusszeile||Fusszeile||||||||','6')    
);

Tx_Flux_Core::registerProviderExtensionKey($_EXTKEY, 'Content');
Tx_Flux_Core::registerProviderExtensionKey($_EXTKEY, 'Page');

?>