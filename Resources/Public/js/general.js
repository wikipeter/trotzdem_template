(function($){
	// returns the topmost value
	$.fn.max = function(selector) {
		return Math.max.apply(null, this.map(function(index, el) { return selector.apply(el); }).get() );
	};

	// actions to take place after dom has loaded
	$(document).ready(function(){
		if ( $('.content .well, .teaser-area .well, .sidebar .well').length ) {
			// fire equalheights after window has loaded
			$(window).load(function() {
				// set min-height to equal other wells in this row
				$('.content .well, .teaser-area .well, .sidebar .well').css('min-height',function () {
					var maxHeight = $(this).parent().parent().find('.well').max( function () {
						return $(this).height();
					});

					return maxHeight;
				});
			});

			// fetch resize() event on windows
			$(window).resize(function() {
				// reset box sizing
				$('.content .well, .teaser-area .well, .sidebar .well').removeAttr('style');

				// adjust min-height again
				$('.content .well, .teaser-area .well').css('min-height',function () {
					var maxHeight = $(this).parent().parent().find('.well').max( function () {
						return $(this).height();
					});

					return maxHeight;
				});
			});
		}
	});
})(jQuery);














