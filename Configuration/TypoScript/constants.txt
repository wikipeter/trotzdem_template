plugin.tx_trotzdemtemplate {
	view {
		templateRootPath = EXT:trotzdem_template/Resources/Private/Templates/
		partialRootPath = EXT:trotzdem_template/Resources/Private/Partials/Page/
		layoutRootPath = EXT:trotzdem_template/Resources/Private/Layouts/
	}
	persistence {
		storagePid = 4
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
	}
}

# custom generic settings
settings {
	project {
		name = TROTZDEM e.V. – Verein für Jugendhilfe
		short = tev
		url = http://trotzdem-ev.de
		templatename = trotzdem_template
		defaultLanguage = 0
		languages = 0,1,2,3
		analyticsID = UA-745495-14

		social {
			email = info@trotzdem-ev.de
			facebook = 
			twitter = 
			youtube = 
		}
	}

	config {
		no_cache = 1
		
		# indexed_search content indexing
		index = 0
		index_externals = 0
		index_meta = 0

		# realurl
		absRefPrefix = /

		# assets compression
		concatenateJs = 0
		concatenateCss = 0
		compressJs = 0
		compressCss = 0
	}
}

# -----------------------------------------------------------------------------
# TypoScript CONSTANTS includes
# -----------------------------------------------------------------------------

# core constants
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:trotzdem_template/Configuration/TypoScript/Core/Constants/vars.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:trotzdem_template/Configuration/TypoScript/Core/Constants/meta.txt">

# plugins constants
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:trotzdem_template/Configuration/TypoScript/Plugins/css_styled_content/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:trotzdem_template/Configuration/TypoScript/Plugins/go_maps_ext/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:trotzdem_template/Configuration/TypoScript/Plugins/bc_services/constants.txt">

# formhandler
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:trotzdem_template/Resources/Public/formhandler/base_form/ts/ts_constants.txt">