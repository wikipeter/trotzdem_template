# -----------------------------------------------------------------------------
# groupAdmin.txt - modified TSConfig for admin users
# -----------------------------------------------------------------------------

# parse templates without the need to clear the cache
admPanel.override.tsdebug.forceTemplateParsing = 1

options.pageTree {
	
	# display domain within pagetree (is root needs to be set in the page properties)
	showDomainNameWithTitle = 1
}